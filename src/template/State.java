package template;

import java.util.List;

import logist.task.Task;
import logist.task.TaskSet;
import logist.topology.Topology.City;


class Transition {
	public enum Type { PICKUP, DELIVER };
	public Task task;
	public Type type;
	
	public Transition(Task task, Type type) {
		this.task = task;
		this.type = type;
	}

	@Override
	public String toString() {
		return type.name() + " " + task.pickupCity.name + " " + task.deliveryCity.name;
	}
}

public class State implements Comparable<State> {
	public City city;
	public TaskSet availableTasks;
	public TaskSet toDeliverTasks;
	public List<Transition> transitions;
	public double availableSpace;
	public double gValue;
	private double hValue = -1;
	
	public double getFValue() {
		return gValue + getHValue();
	}
	
	public double getHValue() {
		if (hValue != -1) {
			return hValue;
		}
		hValue = 0;
		for (Task task : availableTasks) {
			double cost = city.distanceTo(task.pickupCity) + task.pathLength();
			if (cost > hValue) {
				hValue = cost;
			}
		}
		for (Task task : toDeliverTasks) {
			double cost = city.distanceTo(task.deliveryCity);
			if (cost > hValue) {
				hValue = cost;
			}
		}
		return hValue;
	}

	public boolean isEqual(State obj) {
		return city.equals(obj.city) 
				&& availableTasks.equals(obj.availableTasks)
				&& toDeliverTasks.equals(obj.toDeliverTasks);
		}
	

	public boolean isFinal() {
		return availableTasks.isEmpty() && toDeliverTasks.isEmpty();
	}
	
	@Override
	public String toString() {
		return this.city.name + " " + transitions;
	}

	@Override
	public int compareTo(State arg0) {
		return Double.compare(this.getFValue(), arg0.getFValue());
	}
}
