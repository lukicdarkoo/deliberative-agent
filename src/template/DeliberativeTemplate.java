package template;

/* import table */
import logist.simulation.Vehicle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import logist.agent.Agent;
import logist.behavior.DeliberativeBehavior;
import logist.plan.Plan;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.task.TaskSet;
import logist.topology.Topology;
import logist.topology.Topology.City;

/**
 * An optimal planner for one vehicle.
 * Transitions: PICKUP_<city>, DELIVER_<city>
 */
@SuppressWarnings("unused")
public class DeliberativeTemplate implements DeliberativeBehavior {

	enum Algorithm { BFS, ASTAR }
	
	/* Environment */
	Topology topology;
	TaskDistribution td;
	
	/* the properties of the agent */
	Agent agent;
	int capacity;

	/* the planning class */
	Algorithm algorithm;
	boolean autoDeliverPickup;
	
	@Override
	public void setup(Topology topology, TaskDistribution td, Agent agent) {
		this.topology = topology;
		this.td = td;
		this.agent = agent;
		
		// initialize the planner
		int capacity = agent.vehicles().get(0).capacity();
		String algorithmName = agent.readProperty("algorithm", String.class, "ASTAR");
		autoDeliverPickup = agent.readProperty("auto-deliver-pickup", Boolean.class, true);
		
		// Throws IllegalArgumentException if algorithm is unknown
		algorithm = Algorithm.valueOf(algorithmName.toUpperCase());
	}
	
	@Override
	public Plan plan(Vehicle vehicle, TaskSet tasks) {
		Plan plan;

		long startTime = System.currentTimeMillis();
		switch (algorithm) {
		case ASTAR:
			plan = astar(vehicle, tasks);
			break;
		case BFS:
			plan = bfs(vehicle, tasks);
			break;
		default:
			throw new AssertionError("Should not happen.");
		}
		System.out.println("Time taken: " + (System.currentTimeMillis() - startTime) / 1000.0 + "s");
		System.out.println("Plan: " + plan);
		
		return plan;
	}
	
	private Plan naivePlan(Vehicle vehicle, TaskSet tasks) {
		City current = vehicle.getCurrentCity();
		Plan plan = new Plan(current);

		for (Task task : tasks) {
			System.out.println(task.pickupCity);
			// move: current city => pickup location
			for (City city : current.pathTo(task.pickupCity))
				plan.appendMove(city);

			plan.appendPickup(task);

			// move: pickup location => delivery location
			for (City city : task.path())
				plan.appendMove(city);

			plan.appendDelivery(task);

			// set current city
			current = task.deliveryCity;
		}
		return plan;
	}

	@Override
	public void planCancelled(TaskSet carriedTasks) {
		if (!carriedTasks.isEmpty()) {
			// This cannot happen for this simple agent, but typically
			// you will need to consider the carriedTasks when the next
			// plan is computed.
			// Taken care in getInitialState()
		}
	}
	
	private State getInitialState(Vehicle vehicle, TaskSet tasks) {	
		State state = new State();
		state.availableTasks = tasks.clone();
		state.toDeliverTasks = vehicle.getCurrentTasks().clone();
		state.city = vehicle.getCurrentCity();
		state.transitions = new ArrayList<Transition>();
		state.gValue = 0;
		state.availableSpace = vehicle.capacity();
		return state;
	} 
	
    private Plan stateToPlan(State state, Vehicle vehicle) {
    	City previousCity = vehicle.getCurrentCity();
    	Plan plan = new Plan(vehicle.getCurrentCity());
    	
        for (Transition transition : state.transitions) {
            City destination = (transition.type == Transition.Type.DELIVER) ? 
            		transition.task.deliveryCity : transition.task.pickupCity;
            
            for (City c : previousCity.pathTo(destination)) {
                plan.appendMove(c);
            }
            previousCity = destination;
            
            if (transition.type == Transition.Type.DELIVER) {
                plan.appendDelivery(transition.task);
            } else {
                plan.appendPickup(transition.task);
            }
        }
            
        return plan;
    }
	
	private List<State> getSuccessors(State state) {
		List<State> successorStates = new ArrayList<State>();
		
		for (Transition transition : getTransitions(state)) {
			TaskSet availableTasks = state.availableTasks.clone();
			TaskSet toDeliverTasks = state.toDeliverTasks.clone();
			List<Transition> transitions = new ArrayList<Transition>(state.transitions);
			State successorState = new State();
			transitions.add(transition);
			
			if (transition.type == Transition.Type.PICKUP) {	
				availableTasks.remove(transition.task);
				toDeliverTasks.add(transition.task);
				successorState.city = transition.task.pickupCity;
				successorState.availableSpace = state.availableSpace - transition.task.weight;
			}
			else if (transition.type == Transition.Type.DELIVER) {
				toDeliverTasks.remove(transition.task);
				successorState.city = transition.task.deliveryCity;
				successorState.availableSpace = state.availableSpace + transition.task.weight;
			}
			else {
				new Exception("Transion is not supported" + transition.type.toString());
			}
			successorState.availableTasks = availableTasks;
			successorState.toDeliverTasks = toDeliverTasks;
			successorState.transitions = transitions;
			successorState.gValue = state.gValue + state.city.distanceTo(successorState.city);
			successorStates.add(successorState);
		}
		return successorStates;
	}
	
	private List<Transition> getTransitions(State state) {
		List<Transition> transitions = new ArrayList<Transition>();
		for (Task task : state.toDeliverTasks) {
			transitions.add(new Transition(task, Transition.Type.DELIVER));
			if (autoDeliverPickup && task.deliveryCity.equals(state.city)) {
				return Arrays.asList(new Transition(task, Transition.Type.DELIVER));
			}
		}
		for (Task task : state.availableTasks) {
			if (task.weight <= state.availableSpace) {
				transitions.add(new Transition(task, Transition.Type.PICKUP));
				if (autoDeliverPickup && task.pickupCity.equals(state.city)) {
					return Arrays.asList(new Transition(task, Transition.Type.PICKUP));
				}
			}
		}
		return transitions;
	}
	
	private void printStastics(Vehicle vehicle, TaskSet tasks, State state) {
		double sum = 0;
		for (Task task : tasks) {
			sum += task.weight;
		}
		
		System.out.println("Use auto-deliver-pickup to speed up: " + autoDeliverPickup);
		System.out.println("Sum task weight: " + sum);
		System.out.println("Available space: " + vehicle.capacity());
		System.out.println("Initial state avaiable tasks: " + state.availableTasks.size());
		System.out.println("Initial state to deliver tasks: " + state.toDeliverTasks.size());
	}
	
	private Plan astar(Vehicle vehicle, TaskSet tasks) {		
		City currentCity = vehicle.getCurrentCity();
		Plan plan = new Plan(currentCity);
		
		List<State> Q = new ArrayList<State>();
		List<State> C = new ArrayList<State>();
		State n = null;
		
		System.out.println("A* Started");
		printStastics(vehicle, tasks, getInitialState(vehicle, tasks));

		// A*
		int c = 0;
		Q.add(getInitialState(vehicle, tasks));
		do {	
			c++;
			n = Q.remove(0);
			State stateInC = findCopyOfState(C, n);

			if (stateInC == null || n.getHValue() < stateInC.getHValue()) {
				C.add(n);
				//addAndSort(Q, getSuccessors(n));
				Q.addAll(getSuccessors(n));
				Collections.sort(Q);
			}
		} while (Q.isEmpty() == false && n.isFinal() == false);
		
		System.out.println("A* Done (steps " + c + "): " + n.gValue + "km");	
		return stateToPlan(n, vehicle);
	}
	
	private List<State> addAndSort(List<State> states, List<State> newStates) {
		for (State state : newStates) {
			addAndSort(states, state);
		}
		return states;
	}
	
	// Slower than built in sorting algorithm :(
	private List<State> addAndSort(List<State> states, State state) {
		for (int i = 0; i < states.size(); i++) {
			if (states.get(i).getFValue() > state.getFValue()) {
				states.add(i, state);
				return states;
			}
		}
		states.add(state);
		return states;
	}
	
	private State findMinState(List<State> states) {
		State minState = null;
		for (State state : states) {
			if (minState == null || state.gValue < minState.gValue) {
				minState = state;
			}
		}
		return minState;
	}
	
	private State findCopyOfState(List<State> C, State n) {
		for (State state : C) {
			if (state.isEqual(n)) {
				return state;
			}
		}
		return null;
	}
	
	private Plan bfs(Vehicle vehicle, TaskSet tasks) {		
		City currentCity = vehicle.getCurrentCity();
		Plan plan = new Plan(currentCity);
		
		List<State> Q = new ArrayList<State>();
		List<State> C = new ArrayList<State>();
		List<State> solutions = new ArrayList<State>();
		State n = null;
		
		System.out.println("BFS Started");
		printStastics(vehicle, tasks, getInitialState(vehicle, tasks));

		// BFS
		int c = 0;
		int b = 0;
		Q.add(getInitialState(vehicle, tasks));
		do {
			c++;
			n = Q.remove(0);
			if (n.isFinal() == true) {
				solutions.add(n);
			}
			
			State stateInC = findCopyOfState(C, n);
			if (stateInC == null || n.gValue < stateInC.gValue) {
				
				List<State> toRemove = new ArrayList<State>();
				for (State a: Q) {
					if (a.isEqual(n) && a.gValue > n.gValue) {
						toRemove.add(a);
					}
				}
				Q.removeAll(toRemove);				
				
				C.add(n);
				Q.addAll(getSuccessors(n));
			}
		} while (Q.isEmpty() == false);
		n = findMinState(solutions);
		
		System.out.println("BFS Done (steps " + c + "): " + n.gValue + "km");
		
		return stateToPlan(n, vehicle);
	}
}
